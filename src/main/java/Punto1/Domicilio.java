package Punto1;

public class Domicilio {
    private int tipo;

    private String calle;
    private int numero;

    private int numeroCalle;
    private int piso;
    private int departamento;

    private String nombreBarrio;
    private char manzana;
    private int numeroCasa;

    public Domicilio (String calle, int numero){
        this.calle = calle;
        this.numero = numero;
        tipo = 1;
    }
    public Domicilio (int numeroCalle, int piso, int departamento){
        this.numeroCalle = numeroCalle;
        this.piso = piso;
        this.departamento = departamento;
        tipo = 2;
    }
    public Domicilio (String nombreBarrio, char manzana, int numeroCasa){
        this.nombreBarrio = nombreBarrio;
        this.manzana = manzana;
        this.numeroCasa = numeroCasa;
        tipo = 3;
    }

    public String getCalle(){
        return calle;
    }
    public int getNumero(){
        return numero;
    }

    public int getNumeroCalle(){
        return numeroCalle;
    }
    public int getPiso(){
        return piso;
    }
    public int getDepartamento(){
        return departamento;
    }

    public String getNombreBarrio(){
        return nombreBarrio;
    }
    public char getManzana(){
        return manzana;
    }
    public int getNumeroCasa(){
        return numeroCasa;
    }
    public int getTipo(){
        return tipo;
    }
}
