package Punto1;

public class Cliente {
    private String apellido;
    private int dni;
    private String asunto;
    private String correoElectronico;
    private Domicilio domicilio;
    
    public Cliente(String apellido, int dni, String asunto, String correoElectronico, Domicilio domicilio){
        this.apellido = apellido;
        this.dni = dni;
        this.asunto = asunto;
        this.correoElectronico = correoElectronico;
        this.domicilio = domicilio;
    }
    
    public String getApellido(){
        return apellido;
    }
    public int getDni(){
        return dni;
    }
    public String getAsunto(){
        return asunto;
    }
    public String getCorreoElectronico(){
        return correoElectronico;
    }
    public Domicilio getDomicilio(){
        return domicilio;
    }
}
