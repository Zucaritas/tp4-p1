package Punto1;

public class Administracion {
    
    public Cliente nuevoCliente(String apellido, int dni, String causa, String correoElectronico, String calle, int numero){
        Domicilio nuevoDomicilio = new Domicilio(calle, numero);
        Cliente nuevoCliente = new Cliente(apellido, dni, causa, correoElectronico, nuevoDomicilio);
        return nuevoCliente;
    }
    public Cliente nuevoCliente(String apellido, int dni, String causa, String correoElectronico, String nombreBarrio, char manzana, int numeroCasa){
        Domicilio nuevoDomicilio = new Domicilio(nombreBarrio, manzana, numeroCasa);
        Cliente nuevoCliente = new Cliente(apellido, dni, causa, correoElectronico, nuevoDomicilio);
        return nuevoCliente;
    }
    public Cliente nuevoCliente(String apellido, int dni, String causa, String correoElectronico, int numeroCalle, int piso, int departamento){
        Domicilio nuevoDomicilio = new Domicilio(numeroCalle, piso, departamento);
        Cliente nuevoCliente = new Cliente(apellido, dni, causa, correoElectronico, nuevoDomicilio);
        return nuevoCliente;
    }
    public void imprimirCliente(Cliente nuevoCliente){

        Domicilio domicilioCliente = nuevoCliente.getDomicilio();
        int tipo = domicilioCliente.getTipo();

        System.out.println(nuevoCliente.getApellido());
        System.out.println(nuevoCliente.getAsunto());
        System.out.println(nuevoCliente.getCorreoElectronico());
        System.out.println(nuevoCliente.getDni());

        imprimirDomicilio(domicilioCliente, tipo);
    }
    public void imprimirDomicilio(Domicilio domicilioCliente, int tipo){
        switch(tipo){
            case 1: System.out.println(domicilioCliente.getCalle());
                    System.out.println(domicilioCliente.getNumero());
                    break;
            case 2: System.out.println(domicilioCliente.getNumeroCalle());
                    System.out.println(domicilioCliente.getPiso());
                    System.out.println(domicilioCliente.getDepartamento());
                    break;
            case 3: System.out.println(domicilioCliente.getNombreBarrio());
                    System.out.println(domicilioCliente.getManzana());
                    System.out.println(domicilioCliente.getNumeroCasa());
       }
    }

}
